--[[

# moonbird is a simple but powerful http server (beta) with lua 5.2, and sqlite3 embed
# https://gitlab.com/denisdemais/moonbird

dispatcher 'layout':
>> request >>
{
	method: string,
	uri: string,
	[ body: string ],
	[ header: table ],
	socket: int,
	remote_ip: string
	server_ip: string
}

<< response <<
	ok=boolean, http_code=string, http_code_description=string, content_type=string, content=string
	true, "200", "OK", "text/html", "<h1>ok</h1>"
	false, "500", "Internal Server Error", nil, nil

]]--

package.path = 'I:lua/?.lua;I:lua/?/init.lua;lua/?/?.lua?.lua';

-- if (debug) then
-- require 'strict';
-- end

_G.router	= require 'router'      -- https://github.com/APItools/router.lua
_G.json		= require 'dkjson'      -- http://dkolf.de/src/dkjson-lua.fsl/home
_G.sqlite3	= require 'sqlite3'     -- http://lua.sqlite.org/
_G.L		= require "say"         -- https://github.com/lunarmodules/say
_G.M		= require 'moses'       -- https://github.com/Yonaba/Moses
_G.lustache	= require 'lustache'    -- https://github.com/Olivine-Labs/lustache
_G.date		= require 'date'        -- https://github.com/Tieske/date
_G.buffer	= require 'strbuffer'   -- https://github.com/DarkWiiPlayer/lua_strbuffer

_G.requests = {
	counter_400 = 0,
	counter_404 = 0,
};

_G.R		= router.new()
_G.conn		= false;

_G.http_codes = {
	["200"] = "OK",
	["201"] = "Created",
	["202"] = "Accepted",

	["400"] = "Bad Request",
	["401"] = "Unauthorized",
	["404"] = "Not Found",
	["406"] = "Not Acceptable",
	["413"] = "Request Entity Too Large",
	["418"] = "I'm a Teapot",
	["409"] = "Conflict",

	["500"] = "Internal Server Error",
	["501"] = "Not Implemented",
	["503"] = "Service Unavailable"
}

-- ----------------------------------------------------------------------------
-- routes
-- ----------------------------------------------------------------------------

R:get('/', function(params)
	return response_text(true, "200", "moonbird up and running!");
end);

R:get('/info', function(params)
	local t = {
		database	= conn:isopen(),
		lua_memory	= collectgarbage("count"),
		lua_version	= _VERSION,
		os_clock	= os.clock(),
		os_time		= os.time(),
		os_date		= os.date(),
		date		= date():fmt('${iso}')
	};

	return response_json(true, "200", t);
end);

R:get('/lustache/1', function(params)
	local tmplt = [[
	<html><body>
	{{title}} <b>spends</b> {{calc}} steps!
	<body><html>
	]];

	local view_model = {
	  title = "Joe",
	  calc = function () return 2 + 4 end
	};

	return response_lustache(true, "200", tmplt, view_model, true);
end);

R:get('/sqlite3', function(params)
	return response_json(true, "200", {version=sqlite3.version(), lversion=sqlite3.lversion()});
end);

R:get('/conta', function(params)
	-- https://www.sqlite.org/json1.html

	local rows = {}
	local sql;

	sql = [[
	select 
	count(*) as counter, 
	AVG(json_extract(json, '$.total')) AS avg_valor, 
	SUM(json_extract(json, '$.total')) AS sum_valor 
	FROM conta;
	]];

    for row in conn:nrows(sql) do rows[#rows+1] = row; end

	return response_json(true, "200", rows);
end);

R:get('/lang/:id', function(params)
	L:set_namespace(params.id);
	return response_json(true, "200", {money = L('money')} );
end);

R:get('/buffer', function(params)
	local buf = buffer();
	buf:append('the');
	buf:append(' ');
	buf:append('answer');
	buf:append(' ');
	buf:append('is');
	buf:append(' ');
	buf:append(42);

	return response_text(true, "200", tostring(buf));
end);

R:get('/random', function(params)
	return response_text(true, "200", tostring(math.random()))
end);

R:get('/random/:min', function(params)
	params.min = tointeger(params.min);
	return response_text(true, "200", tostring(math.random(params.min)))
end);

R:get('/random/:min/:max', function(params)
	params.min = tointeger(params.min);
	params.max = tointeger(params.max);
	return response_text(true, "200", tostring(math.random(params.min, params.max)))
end);

R:get('/metrics', function(params)
	local tmplt = [[
		# Uptime
		uptime {{ uptime }}

		# Contador de requisições HTTP
		http_requests_total{status="400"} {{ counter_400 }}
		http_requests_total{status="404"} {{ counter_404 }}
	]];

	local model = {
		uptime = os.clock(),
		counter_400 = requests.counter_400,
		counter_404 = requests.counter_404,
	}

	return response_lustache(true, "200", tmplt, model, true);
end);

R:get('/f57694e0-d7e7-42c5-b7be-a36130561d3d', function(params)
	requests.counter_400 = requests.counter_400+1;
	return response_empty(false, "400");
end);

R:get('/cf0b9c49-1586-47a0-9fe6-f442ffdfd724', function(params)
	requests.counter_404 = requests.counter_404+1;
	return response_empty(false, "404");
end);

R:get('/73ca4788-9ad1-4e15-9552-becf443e2c96', function(params)
	return response_empty(false, "500");
end);

R:get('/77bcb8bd-b1d4-42ef-906e-1ac72e1be5d6', function(params)
	return response_empty(false, "501");
end);

--
-- pessoa
--

--
-- get all
-- { {id,nome}, ... }
--
R:get('/v1/pessoas', function(params)
	local rows = {}
	local sql = "select * from pessoa_view ORDER BY id";
	
    for row in conn:nrows(sql) do rows[#rows+1] = row; end

	if (M.isEmpty(rows)) then return response_empty(false, "404"); end

	return response_json(true, "200", rows);
end);

--
-- get paged
-- { {id,nome}, ... }
--
R:get('/v1/pessoas/:limit/:OFFSET', function(params)
	local rows = {}
	local sql = string.format(
		"select * from pessoa_view ORDER BY id LIMIT %d OFFSET %d",
		params.limit,
		params.offset
	);

    for row in conn:nrows(sql) do rows[#rows+1] = row; end

	return response_json(true, "200", rows);
end);

--
-- get one
-- { id, nome }
--
R:get('/v1/pessoa/:id', function(params)
	local rows = {}

	local sql = string.format(
		"select * from pessoa_view where id=%d LIMIT 1",
		params.id
	);
	
    for row in conn:nrows(sql) do rows[#rows+1] = row; end
	
	if (M.isEmpty(rows)) then return response_empty(false, "404"); end

	return response_json(true, "200", rows[1]);
end);

--
-- create one
-- { nome }
--
R:post('/v1/pessoa', function(params)
	local obj, sql;

	obj = json.decode(params.body);
	if (not obj) then return response_empty(false, "400"); end
	if (type(params.body) ~= 'string') then return response_empty(false, "400"); end

	params.created_at = date():fmt('${iso}');

	sql = string.format("INSERT INTO pessoa (nome, created_at) VALUES('%s', '%s')", obj.nome, params.created_at);
	if (conn:exec(sql) ~= sqlite3.OK) then return response_empty(false, "500"); end	

	return response_json(true, "201", {id = conn:last_insert_rowid()});
end);

--
-- update one
-- { id, nome }
--
R:put('/v1/pessoa/:id', function(params)
	local obj, sql;

	obj = json.decode(params.body);
	if (not obj) then return response_empty(false, "400"); end

	params.id = tointeger(params.id);
	if (not params.id) then return response_empty(false, "400"); end
	if (type(params.body) ~= 'string') then return response_empty(false, "400"); end

	params.now = date():fmt('${iso}');

	sql = string.format("UPDATE pessoa SET nome='%s',updated_at='%s' where id=%d and deleted=0", obj.nome, params.now, params.id);
	if (conn:exec(sql) ~= sqlite3.OK) then return response_empty(false, "404"); end	

	return response_json(true, "202", {changes = conn:changes(), total_changes = conn:total_changes()});
end);

--
-- soft delete one
-- { id }
--
R:delete('/v1/pessoa/:id', function(params)
	local obj, sql;

	params.id = tointeger(params.id);
	if (not params.id) then return response_empty(false, "400"); end

	params.now = date():fmt('${iso}');

	sql = string.format("UPDATE pessoa SET deleted=1,updated_at='%s' where id=%d and deleted=0", params.now, params.id);
	if (conn:exec(sql) ~= sqlite3.OK) then return response_empty(false, "404"); end	

	return response_json(true, "202", {changes = conn:changes(), total_changes = conn:total_changes()});
end);

--
-- hard delete
--
R:delete('/v1/pessoa', function(params)
	local obj, sql;

	params.now = date():fmt('${iso}');

	sql = string.format("DELETE from pessoa");
	if (conn:exec(sql) ~= sqlite3.OK) then return response_empty(false, "404"); end	

	return response_json(true, "202", {changes = conn:changes(), total_changes = conn:total_changes()});
end);

--
-- 
--

function response_any(ok, http_code, any)
	if (type(any) == 'string') then
		return response_text(ok, http_code, any);
	end

	if (type(any) == 'table') then
		return response_json(ok, http_code, any);
	end

	return response_empty(ok, http_code);
end

function response_empty(ok, http_code)
	return ok, http_code, http_codes[http_code], nil, nil;
end

function response_lustache(ok, http_code, tmplt, view_model, strip)
	local render;
	
	if (strip) then
		render = lustache:render(tmplt:gsub("\t", ""), view_model);
	else
		render = lustache:render(tmplt, view_model);
	end

	return response_html(ok, http_code, render);
end

function response_html(ok, http_code, html)
	return ok, http_code, http_codes[http_code], "text/html; charset=utf-8", html;
end

function response_text(ok, http_code, text)
	return ok, http_code, http_codes[http_code], "text/plain; charset=utf-8", tostring(text);
end

function response_json(ok, http_code, tbl, indent)
	local encoded = json.encode(tbl, {indent = indent});
	return ok, http_code, http_codes[http_code], "application/json; charset=utf-8", encoded;
end

--
--
--

function tointeger(n)
	if (type(n) ~= 'number') then n = tostring(n); end

	n = tonumber(n);
	if (not n) then return 0; end
	
    return n<0 and math.ceil(n) or math.floor(n);
end

--
--
--

function dispatcher(params)
	local fn, args

	if (not conn:isopen()) then
		fn, args = R:resolve("GET", "/73ca4788-9ad1-4e15-9552-becf443e2c96");
		return fn(args);
	end
	
	fn, args = R:resolve(params.method, params.uri, params);
	if (fn) then return fn(args); end

	fn, args = R:resolve("GET", "/cf0b9c49-1586-47a0-9fe6-f442ffdfd724");
	return fn(args);
end

function setup()
	-- transient in-memory database
	_G.conn = sqlite3.open_memory();

	if (conn:isopen()) then
		conn:exec([[
		pragma journal_mode = memory;
		pragma temp_store = memory;
		PRAGMA LOCKING_MODE = EXCLUSIVE;
		pragma synchronous = normal;
		PRAGMA foreign_keys = OFF;

		CREATE TABLE          IF NOT EXISTS pessoa(id INTEGER PRIMARY KEY AUTOINCREMENT, nome text NOT NULL, created_at text NOT NULL, updated_at text, deleted INTEGER NOT NULL DEFAULT 0);
		CREATE INDEX          IF NOT EXISTS pessoa_deleted_index ON pessoa(deleted);
		CREATE TEMPORARY VIEW IF NOT EXISTS pessoa_view AS select id, nome from pessoa WHERE deleted=0;

		CREATE TABLE conta (id INTEGER, json TEXT);
		INSERT INTO conta (id, json) VALUES (1, '{"pessoas": 1, "total": 5.0}');
		INSERT INTO conta (id, json) VALUES (1, '{"pessoas": 3, "total": 10.0}');
		INSERT INTO conta (id, json) VALUES (2, '{"pessoas": 2, "total": 7.5}');

		pragma vacuum;
		pragma optimize;
		]]);
	end

	-- faker --
	-- https://www.name-generator.org.uk/hero/
	local names = {
		'Mae Duke',
		'Spencer Strickland',
		'Adele Bradley',
		'Beth Peters (Ilsa',
		'Rebekah Hart (Becca)',
		'Stella Maldonado',
		'Lilly Sheppard (Lil)',
		'Emma Mcdonald',
		'Yasmine Bean',
		'Anya Guzman',
		'Kian Chen',
		'Maya Hines',
		'Madeline Walker',
		'Anastasia Byrne (Stacie)',
		'Amber Griffith',
		'Aliya Webb',
	};

	for i=1, #names do
		local name = names[i];
		local created_at = date():fmt('${iso}');
		local deleted = math.random(0,1);

		local sql = 'INSERT INTO pessoa (nome,created_at,deleted) VALUES (%q,%q,%d)';
		conn:exec(string.format(sql, name, created_at,deleted));
	end
	-- faker ---

	L:set_fallback('en');
	L:set_namespace("en");
	L:set('money', 'money');

	L:set_namespace("fr");
	L:set('money', 'argent');

	L:set_namespace("jp");
	L:set('money', 'okane');

	L:set_namespace("ga");
	L:set('money', 'diñeiro');

	L:set_namespace("la");
	L:set('money', 'pecunia');

	L:set_namespace("zu");
	L:set('money', 'imali');

	L:set_namespace("br");
	L:set('money', 'dinheiro');
end

setup();
collectgarbage();
