local function tprint (t, indent, done, str)
	
  local Note, Tell;
  
  -- in case we run it standalone
  Note = Note or print;
  Tell = Tell or io.write;
  
  -- show strings differently to distinguish them from numbers
  local function show (val)
    if type (val) == "string" then
      return '"' .. val .. '"';
    else
      return tostring (val);
    end
  end
  
  done = done or {};
  indent = indent or 0;
  for key, value in pairs (t) do
    Tell (string.rep (" ", indent));
    if type (value) == "table" and not done [value] then
      done [value] = true
      Note (show (key), ":");
      tprint (value, indent + 2, done);
    else
      Tell (show (key), "=");
      Note (show (value));
    end
  end
  
end

return tprint;
