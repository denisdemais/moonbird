# a simple but powerful http server (beta) with lua 5.2, and sqlite3 embed

## inspired on a old project based on:
	https://www.apache.org
	https://www.php.org
	https://www.nodejs.org
	https://github.com/slimphp/Slim/tree/2.x
	https://github.com/Philipp15b/php-i18n
	https://redbeanphp.com/index.php
	https://mustache.github.io/
	https://thephpleague.com/pt-br/

## libraries/info used in this demo:
	https://www.lua.org
	http://www.sqlite.org
	https://www.lua.org/extras/5.2/
	https://github.com/APItools/router.lua
	http://dkolf.de/src/dkjson-lua.fsl/home
	http://lua.sqlite.org/index.cgi/doc/tip/doc/lsqlite3.wiki
	https://github.com/lunarmodules/say
	https://github.com/Olivine-Labs/lustache
	https://github.com/Tieske/date
	https://github.com/DarkWiiPlayer/lua_strbuffer/
	https://developer.mozilla.org/en-US/docs/Web/HTTP/Status#information_responses

## image splash:
	https://www.bing.com/create

### run on windows, mac?, linux and verix =)

# thanks for all!

