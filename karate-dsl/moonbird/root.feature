Feature:

  Background:
    * configure report = {showLog: false, showAllSteps: false}
    * url 'http://192.168.0.101:80'

  Scenario: get root
    Given path '/'
    When method GET
    Then status 200

  Scenario: get info
    Given path '/info'
    When method GET
    Then status 200

  Scenario: get metrics
    Given path '/metrics'
    When method GET
    Then status 200

  Scenario: get a random number
    Given path '/random'
    When method GET
    Then status 200

  Scenario: get a random number up to 100
    Given path '/random/100'
    When method GET
    Then status 200

  Scenario: get a random number between 10 and 20
    Given path '/random/10/20'
    When method GET
    Then status 200

  Scenario: get error 400
    Given path '/f57694e0-d7e7-42c5-b7be-a36130561d3d'
    When method GET
    Then status 400

  Scenario: get error 404
    Given path '/cf0b9c49-1586-47a0-9fe6-f442ffdfd724'
    When method GET
    Then status 404

  Scenario: get error 500
    Given path '/73ca4788-9ad1-4e15-9552-becf443e2c96'
    When method GET
    Then status 500

  Scenario: get error 501
    Given path '/77bcb8bd-b1d4-42ef-906e-1ac72e1be5d6'
    When method GET
    Then status 501
