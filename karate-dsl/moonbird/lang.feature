Feature: lang

  Background:
    * url 'http://192.168.0.101:80'

  Scenario:
    Given path '/lang/en'
    When method GET
    Then status 200
	And match $..money contains '#notnull'
	And match $.money == 'money'

  Scenario:
    Given path '/lang/br'
    When method GET
    Then status 200
	And match $.money == 'dinheiro'
